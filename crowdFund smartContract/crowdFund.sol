// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract crowdFund {

mapping(address=>uint) public contributors;
address public manager;
uint public minContribution;
uint public target;
uint public deadline;
uint public raisedAmount;
uint public noOfcontributors;

struct Request{
    string description;
    address payable recipient;
    uint value;
    bool completed;
    uint noOfVoters;
    mapping(address=>bool) voters;

}

   constructor(uint _target,uint _deadline)  {
        target=_target;
        deadline=block.timestamp+_deadline; //10sec + 3600sec (60*60)
        minContribution=100 wei;
        manager=msg.sender;
    }
mapping(uint=>Request) public requests;   
uint public numOfrequest;


    function sendeth() public payable {
        require(block.timestamp<deadline,"deadline has reached");
        require(msg.value >= minContribution,"minimum contribution is not met");

        if(contributors[msg.sender]==0){
            noOfcontributors++;
        }

        contributors[msg.sender]+=msg.value;
        raisedAmount+=msg.value;
    }

    function getContractBalance() public view returns(uint){

        return address(this).balance;
    }

    function refund() public {
        require(block.timestamp > deadline || raisedAmount < target,"you are not yet eligible for a refund");
        require(contributors[msg.sender]>0);
        address payable user = payable(msg.sender);
        user.transfer(contributors[msg.sender]);
        contributors[msg.sender]=0;

    }

    modifier onlyManager(){
        require(msg.sender==manager,"only manager can call this function");
        _;
    }

    function createRequest(string memory _description,address payable _recipient,uint _value)public onlyManager {
        Request storage newRequest = requests[numOfrequest];
        numOfrequest++;
        newRequest.description = _description;
        newRequest.recipient=_recipient;
        newRequest.value=_value;
        newRequest.completed=false;
        newRequest.noOfVoters=0;

    }


    function voterRequest(uint _requestNo) public {
        require(contributors[msg.sender]>0,"you must be a contributor to vote");
        Request storage thisRequest = requests[_requestNo];
        require(thisRequest.voters[msg.sender]==false,"you have already voted");
        thisRequest.voters[msg.sender]=true;
        thisRequest.noOfVoters++;


    }
    
    function makePayment(uint _requestNo) public onlyManager{
        require(raisedAmount>=target);
        Request storage thisRequest=requests[_requestNo];
        require(thisRequest.completed==false,"The request has been completed");
        require(thisRequest.noOfVoters > noOfcontributors/2,"Majority does not support");
        thisRequest.recipient.transfer(thisRequest.value);
        thisRequest.completed=true;
    }

}
